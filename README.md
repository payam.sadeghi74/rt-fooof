# RT-fooof : Parameterizing Cortical Power Spectra in Real Time

This repository designed for real time implementation of the Fitting Oscillations & One Over F (fooof) algorithm for advanced spectral analysis of magneto and electroencephalography (M/EEG) signals in Brain-Computer Interfaces (BCIs).

Tested with:

- Simulated data
- Fake streamed resting-state recording
- Real-time recording 

Also includes:

- Comparison with classical FFT
- Simulating M/EEG recordings with adjustable activity in desired frequency band (e.g. alpha)

## Dependencies

The minimum required dependencies to run RT-fooof are:

- Python >= 3.8
- NumPy >= 1.20.2
- SciPy >= 1.6.3
- fooof >= 1.0.0
- BSL >= 0.8

## Citation
If you find RT-fooof useful in your research, please cite: (not published yet)

```
@inproceedings{shabestari2023parameterized,
  title={Parameterized Cortical Power Spectra as a Novel Neural Feature for Real Time BCI},
  author={Shabestari, Payam S and Kleinjung, Tobias and Neff, Patrick},
  booktitle={2023 IEEE Biomedical Circuits and Systems Conference (BioCAS)},
  pages={1--4},
  year={2021},
  organization={IEEE}
}
```

